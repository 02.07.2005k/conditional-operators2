"use strict"
// 1) 
//Логічний оператор - це символ або слово, що використовується для об'єднання, 
//порівняння або зміни значень булевих виразів у програмуванні,
// щоб визначити істинність чи хибність виразу.

// 2)
//У JavaScript існують три основних логічних оператори: 
//AND (логічне "І"), OR (логічне "АБО"), 
//і NOT (логічне "НЕ"). 
//Символи для цих операторів - && для AND, || для OR, і ! для NOT.

// Практичне   Завдання 1
let age = prompt("Введіть ваш вік:");

if (age < 12) {
    alert("Ви є дитиною.");
} else if (age < 18) {
    alert("Ви є підлітком.");
} else {
    alert("Ви є дорослим.");
}
// Практичне   Завдання 2
function daysInMonth(month) {
    switch (month) {
        case 'січень':
        case 'березень':
        case 'травень':
        case 'липень':
        case 'серпень':
        case 'жовтень':
        case 'грудень':
            return 31;
        case 'квітень':
        case 'червень':
        case 'вересень':
        case 'листопад':
            return 30;
        case 'лютий':
            return 'У нормальному році 28 днів, у високосний рік - 29';
        default:
            return 'Неправильно введений місяць.';
    }
}

let month = prompt('Введіть назву місяця українською мовою (наприклад: січень, лютий і т.д.):').toLowerCase();

let days = daysInMonth(month);
alert(days);